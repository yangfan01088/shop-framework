-- allocation
       配置相关
-- controller
       相关的控制层
-- interceptor
       springshiro拦截器
-- resolver
       异常标注类


       案例没有加spring拦截器，只是简单的demo


--  resources
      -- config
         -- applicationfoConfig.properties
            项目相关配置
      -- ApplicationContent.xml
         spring配置
      -- ApplicationContent-mvc.xml
         springmvc 集成配置
      -- fdfs_client.conf
         fdfs 图片处理服务器配置
      -- log4j.properties,log4j.xml 日志配置
      -- remoting.xml
         -- 客户端 服务订阅配置